Orbstream PHP SDK
====================
> This is the client SDK for Orbstream service

Install
================
> In your project directory run the following command `composer require orbscope/orbstream`

Usage
=============

```php
<?php
	include "vendor/autoload";

	// basic configs
	$baseUrl = "http://localhost:8080/";
	$appId = "01980671c6f7e71af9aadb624847005b41915f4c4425e8cf2a11731b3e0b3fd6f39d75a9c88e22b81bceccb1ab8a30ad34188601114e10fb1fa8b94073829ed7";
	$appSecret = "8fd7199214954ffbbd3c7876e05a74d7e0ec57b8f1e468045ae3fdf3a9f82540249890f9b6a29357e9c78873e4d5fb8d2dc2b0fe29494d1a9c021f1827795a74";

	// create the client
	$client = new \Orbscope\Orbstream\Client(
		$baseUrl,
		$appId,
		$appSecret
	);

	// publish message
	$client->publish("mychannel", "hello");

	// get a uniqid
	echo $client->uniqid();
```
